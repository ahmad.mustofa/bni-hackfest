-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 11, 2017 at 10:27 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bni`
--

-- --------------------------------------------------------

--
-- Table structure for table `antrian`
--

CREATE TABLE `antrian` (
  `ID_ANTRIAN` int(11) NOT NULL,
  `ID_PELANGGAN` int(11) NOT NULL,
  `ID_JENIS_ANTRIAN` int(11) NOT NULL,
  `ID_BANK` int(11) NOT NULL,
  `JAM_ANTRI` varchar(50) NOT NULL,
  `TANGGAL_ANTRI` date NOT NULL,
  `PERKIRAAN_WAKTU` varchar(50) NOT NULL,
  `STATUS_ANTRIAN` varchar(50) NOT NULL,
  `ID_PETUGAS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `antrian`
--

INSERT INTO `antrian` (`ID_ANTRIAN`, `ID_PELANGGAN`, `ID_JENIS_ANTRIAN`, `ID_BANK`, `JAM_ANTRI`, `TANGGAL_ANTRI`, `PERKIRAAN_WAKTU`, `STATUS_ANTRIAN`, `ID_PETUGAS`) VALUES
(8, 5, 2, 2, '06:26:02am', '2017-03-11', '12', 'ANTRI', 0),
(9, 6, 2, 1, '08:13:46am', '2017-03-11', '12', 'SELESAI', 0),
(10, 10, 1, 1, '08:42:33am', '2017-03-11', '12', 'ANTRI', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `ID_BANK` int(11) NOT NULL,
  `NAMA_BANK` varchar(100) NOT NULL,
  `LATITUDE` double NOT NULL,
  `LONGITUDE` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`ID_BANK`, `NAMA_BANK`, `LATITUDE`, `LONGITUDE`) VALUES
(1, 'BNI UNIBRAW', -7.9580783, 112.5932199),
(2, 'BNI MATOS', -7.9580776, 112.5902199),
(3, 'BNI SAWOJAJAR', -7.9508379, 112.5947256),
(4, 'BNI ITN-2', -7.9508379, 112.5947256),
(5, 'BNI LAWANG', -7.8379042, 112.6905716);

-- --------------------------------------------------------

--
-- Table structure for table `data_cif`
--

CREATE TABLE `data_cif` (
  `No_CIF` int(11) NOT NULL,
  `NAMA` varchar(100) NOT NULL,
  `NAMA_IBU` varchar(100) NOT NULL,
  `TEMPAT_LAHIR` varchar(100) NOT NULL,
  `TANGGAL_LAHIR` date NOT NULL,
  `JENIS_KELAMIN` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jenis_antrian`
--

CREATE TABLE `jenis_antrian` (
  `ID_JENIS_ANTRIAN` int(11) NOT NULL,
  `ID_BANK` int(11) NOT NULL,
  `NAMA_ANTRIAN` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_antrian`
--

INSERT INTO `jenis_antrian` (`ID_JENIS_ANTRIAN`, `ID_BANK`, `NAMA_ANTRIAN`) VALUES
(1, 1, 'CS'),
(2, 1, 'Teller'),
(3, 2, 'CS'),
(4, 2, 'Teller'),
(5, 3, 'CS'),
(6, 3, 'Teller'),
(7, 4, 'CS'),
(8, 4, 'Teller'),
(9, 5, 'CS'),
(10, 5, 'Teller');

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `ID_PELANGGAN` int(11) NOT NULL,
  `NAMA_PELANGGAN` varchar(100) NOT NULL,
  `TEMPAT_LAHIR` varchar(50) NOT NULL,
  `TANGGAL_LAHIR` date NOT NULL,
  `ALAMAT` text NOT NULL,
  `AGAMA` varchar(50) NOT NULL,
  `STATUS_PERKAWINAN` varchar(50) NOT NULL,
  `PEKERJAAN` varchar(100) NOT NULL,
  `NOMOR_HP` varchar(50) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `PASSWORD` varchar(100) NOT NULL,
  `FOTO` varchar(100) NOT NULL,
  `STATUS` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`ID_PELANGGAN`, `NAMA_PELANGGAN`, `TEMPAT_LAHIR`, `TANGGAL_LAHIR`, `ALAMAT`, `AGAMA`, `STATUS_PERKAWINAN`, `PEKERJAAN`, `NOMOR_HP`, `EMAIL`, `PASSWORD`, `FOTO`, `STATUS`) VALUES
(5, 'Diko Andri Vidian', 'malang', '1996-05-03', 'Dampit', 'islam', 'belum menikah', 'pelajar', '085607059697', 'saamdik@gmail.com', 'a', 'definisi data atribut.jpg', 'SUDAH'),
(6, 'diko', 'as', '2016-12-30', 'as', 'kristen', 'menikah', 'asdf', '0987', 'samdik@gmail.com', 'aaaa', '852116927_85363_17925180257242342832.jpg', 'SUDAH'),
(10, 'diko', 'as', '2016-11-30', 'as', 'islam', 'belum menikah', 'asdf', '0987', 'diko@gmail.com', 'as', 'photo6163237731883394991.jpg', 'BELUM');

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `ID_PETUGAS` int(11) NOT NULL,
  `NAMA_PETUGAS` varchar(100) NOT NULL,
  `PASSWORD` varchar(100) NOT NULL,
  `ID_BANK` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`ID_PETUGAS`, `NAMA_PETUGAS`, `PASSWORD`, `ID_BANK`) VALUES
(1, 'JOKO', 'joko', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `antrian`
--
ALTER TABLE `antrian`
  ADD PRIMARY KEY (`ID_ANTRIAN`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`ID_BANK`);

--
-- Indexes for table `jenis_antrian`
--
ALTER TABLE `jenis_antrian`
  ADD PRIMARY KEY (`ID_JENIS_ANTRIAN`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`ID_PELANGGAN`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`ID_PETUGAS`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `antrian`
--
ALTER TABLE `antrian`
  MODIFY `ID_ANTRIAN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `ID_BANK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `jenis_antrian`
--
ALTER TABLE `jenis_antrian`
  MODIFY `ID_JENIS_ANTRIAN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `ID_PELANGGAN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
