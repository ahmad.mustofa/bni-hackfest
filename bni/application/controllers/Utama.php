<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Utama extends CI_Controller {
public function __construct()
	 {
		parent::__construct();
		setlocale(LC_ALL, 'IND');
		$this->load->library('encrypt');
		//$this->load->model('Bni');
		$this->load->helper('form','url');
	 }
	public function Index()
	{
		$this->load->view('Header');
		$this->load->view('Admin/Menu');
		$this->load->view('Admin/Home');
		$this->load->view('Footer');
		
	}
	function Bank(){
		$this->load->view('Header');
		$this->load->view('Admin/Menu');
		$this->load->view('Admin/Bank');
		$this->load->view('Footer');
		
	}
}
