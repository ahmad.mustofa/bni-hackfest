<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>BNI 46</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
  <!-- page stylesheets -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/app/vendor/datatables/media/css/jquery.dataTables.css">
  <!-- end page stylesheets -->
  <!-- build:css({.tmp,app}) styles/app.min.css -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/app/styles/webfont.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/app/styles/climacons-font.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/app/vendor/bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/app/styles/font-awesome.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/app/styles/card.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/app/styles/sli.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/app/styles/animate.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/app/styles/app.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/app/styles/app.skins.css">
  <link href="<?php echo base_url(); ?>template/app/images/logo2.png" rel='icon' type='image/x-icon'/>
  <!-- endbuild -->
</head>
