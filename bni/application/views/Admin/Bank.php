 <div class="main-panel">
      <!-- top header -->
      <div class="header navbar">
        <div class="brand visible-xs">
          <!-- toggle offscreen menu -->
          <div class="toggle-offscreen">
            <a href="javascript:;" class="hamburger-icon visible-xs" data-toggle="offscreen" data-move="ltr">
              <span></span>
              <span></span>
              <span></span>
            </a>
          </div>
          <!-- /toggle offscreen menu -->
          <!-- logo -->
          <a class="brand-logo">
            <span>REACTOR</span>
          </a>
          <!-- /logo -->
        </div>
        <ul class="nav navbar-nav hidden-xs">
          <li>
            <a href="javascript:;" class="small-sidebar-toggle ripple" data-toggle="layout-small-menu">
              <i class="icon-toggle-sidebar"></i>
            </a>
          </li>
         
        </ul>
        <ul class="nav navbar-nav navbar-right hidden-xs">
            
          <li>
            <a href="javascript:;" class="ripple" data-toggle="dropdown">
              <img src="<?php echo base_url(); ?>template/app/images/avatar.jpg" class="header-avatar img-circle" alt="user" title="user">
              <span>Selamat Datang, Admin</span>
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
              <li>
                <a href="javascript:;">Settings</a>
              </li>
              <li>
                <a href="javascript:;">Help</a>
              </li>
              
              <li role="separator" class="divider"></li>
            
              <li>
                <a href="extras-signin.html">Logout</a>
              </li>
            </ul>
          </li>
         
        </ul>
      </div>
      <!-- /top header -->
      <!-- main area -->
       <div class="main-content">
        <div class="page-title">
          <div class="title">Editable</div>
          <div class="sub-title">UI editable datatables</div>
        </div>
        <div class="card bg-white">
          <div class="card-header">
            Datatables
          </div>
          <div class="card-block">
            <table class="table table-bordered table-striped datatable editable-datatable responsive align-middle bordered">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Position</th>
                  <th>Office</th>
                  <th>Age</th>
                  <th>Start Date</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                
				<tr>
				
                 <td></td>
                 <td></td>
				 <td></td>
				 <td></td>
				 <td></td>
                  <td><a href="javascript:;" class="edit">Edit</a>
                  </td>
                  <td><a href="javascript:;" class="delete">Delete</a>
                  </td>
				  
                </tr>
				
               
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /main area -->
    </div>
      <!-- /main area -->
    </div>