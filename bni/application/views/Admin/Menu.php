
<body class="page-loading">
  <!-- page loading spinner -->
  <div class="pageload">
    <div class="pageload-inner">
      <div class="sk-wave">
	  <div class="sk-rect sk-rect1"></div>
	  <div class="sk-rect sk-rect2"></div>
	  <div class="sk-rect sk-rect3"></div>
	  <div class="sk-rect sk-rect4"></div>
		  <div class="sk-rect sk-rect5"></div>
	  </div>
    </div>
  </div>
  <!-- /page loading spinner -->
  <div class="app layout-fixed-header">
    <!-- sidebar panel -->
    <div class="sidebar-panel offscreen-left">
      <div class="brand">
        <!-- toggle small sidebar menu -->
        
        <a class="brand-logo">
          <img src="<?php echo base_url(); ?>template/app/images/logo.png" style="width: 45%; height: 40%; margin: auto; display: block;">
        </a>
        <a href="#" class="small-menu-visible brand-logo"></a>
        <!-- /logo -->
      </div>
     
      <!-- main navigation -->
      <nav role="navigation">
        <ul class="nav">
          <!-- dashboard -->
          <li>
            <a href="<?php echo site_url('utama/index'); ?>">
              <i class="icon-home"></i>
              <span>Beranda</span>
            </a>
          </li>
       
          <li>
            <a href="<?php echo site_url('utama/bank'); ?>">
              <span class="badge pull-right">4</span>
              <i class="icon-layers"></i>
              <span>Data</span>
            </a>
            <ul class="sub-menu">
              <li>
                <a href="<?php echo site_url('utama/bank'); ?>">
                  <span>Bank</span>
                </a>
              </li>
              <li>
                <a href="cards-portlets.html">
                  <span>Portlets</span>
                </a>
              </li>
              <li>
                <a href="cards-draggable.html">
                  <span>Draggable</span>
                </a>
              </li>
              <li>
                <a href="cards-widgets.html">
                  <span>Widgets</span>
                </a>
              </li>
            </ul>
          </li>
          <!-- /cards -->
          <!-- apps -->
        
        </ul>

      </nav>
      <!-- /main navigation -->
    </div>