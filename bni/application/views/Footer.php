 <footer class="content-footer">
      <nav class="footer-right">
        <ul class="nav">
         
        </ul>
      </nav>
      <nav class="footer-left hidden-xs">
        <ul class="nav">
          <li>
            <a href="javascript:;"><span>Copyright 2017 &copy; <b>3 Sekawan</b> </span> | Teknik Informatika UIN Malang</a>
          </li>
          
        </ul>
      </nav>
    </footer>
    <!-- /bottom footer -->
    
    <!-- /chat -->
  </div>
  <!-- build:js({.tmp,app}) scripts/app.min.js -->
  <script src="<?php echo base_url(); ?>template/app/scripts/helpers/modernizr.js"></script>
  <script src="<?php echo base_url(); ?>template/app/vendor/jquery/dist/jquery.js"></script>
  <script src="<?php echo base_url(); ?>template/app/vendor/bootstrap/dist/js/bootstrap.js"></script>
  <script src="<?php echo base_url(); ?>template/app/vendor/fastclick/lib/fastclick.js"></script>
  <script src="<?php echo base_url(); ?>template/app/vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
  <script src="<?php echo base_url(); ?>template/app/scripts/helpers/smartresize.js"></script>
  <script src="<?php echo base_url(); ?>template/app/scripts/constants.js"></script>
  <script src="<?php echo base_url(); ?>template/app/scripts/main.js"></script>
  <!-- endbuild -->
  <!-- page scripts -->
  <script src="<?php echo base_url(); ?>template/app/vendor/datatables/media/js/jquery.dataTables.js"></script>
  <!-- end page scripts -->
  <!-- initialize page scripts -->
  <script src="<?php echo base_url(); ?>template/app/scripts/helpers/bootstrap-datatables.js"></script>
  <script src="<?php echo base_url(); ?>template/app/scripts/tables/table-edit.js"></script>
  <!-- end initialize page scripts -->
  <script src="<?php echo base_url(); ?>template/app/vendor/flot/jquery.flot.js"></script>
  <script src="<?php echo base_url(); ?>template/app/vendor/flot/jquery.flot.resize.js"></script>
  <script src="<?php echo base_url(); ?>template/app/vendor/flot/jquery.flot.categories.js"></script>
  <script src="<?php echo base_url(); ?>template/app/vendor/flot/jquery.flot.stack.js"></script>
  <script src="<?php echo base_url(); ?>template/app/vendor/flot/jquery.flot.time.js"></script>
  <script src="<?php echo base_url(); ?>template/app/vendor/flot/jquery.flot.pie.js"></script>
  <script src="<?php echo base_url(); ?>template/app/vendor/flot-spline/js/jquery.flot.spline.js"></script>
  <script src="<?php echo base_url(); ?>template/app/vendor/flot.orderbars/js/jquery.flot.orderBars.js"></script>
  <!-- end page scripts -->
  <!-- initialize page scripts -->
  <script src="<?php echo base_url(); ?>template/app/scripts/helpers/sameheight.js"></script>
  <script src="<?php echo base_url(); ?>template/app/scripts/ui/dashboard.js"></script>
</body>

</html>