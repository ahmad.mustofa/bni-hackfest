 <footer class="content-footer">
      <nav class="footer-right">
        <ul class="nav">
         
        </ul>
      </nav>
      <nav class="footer-left hidden-xs">
        <ul class="nav">
          <li>
            <a href="javascript:;"><span>Copyright 2017 &copy; <b>3 Sekawan</b> </span> | Teknik Informatika UIN Malang</a>
          </li>
          
        </ul>
      </nav>
    </footer>
    <!-- /bottom footer -->
    
    <!-- /chat -->
  </div>
  <!-- build:js({.tmp,app}) scripts/app.min.js -->
  <script src="scripts/helpers/modernizr.js"></script>
  <script src="vendor/jquery/dist/jquery.js"></script>
  <script src="vendor/bootstrap/dist/js/bootstrap.js"></script>
  <script src="vendor/fastclick/lib/fastclick.js"></script>
  <script src="vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
  <script src="scripts/helpers/smartresize.js"></script>
  <script src="scripts/constants.js"></script>
  <script src="scripts/main.js"></script>
  <!-- endbuild -->
  <!-- page scripts -->
  <script src="vendor/datatables/media/js/jquery.dataTables.js"></script>
  <!-- end page scripts -->
  <!-- initialize page scripts -->
  <script src="scripts/helpers/bootstrap-datatables.js"></script>
  <script src="scripts/tables/table-edit.js"></script>
  <!-- end initialize page scripts -->
</body>

</html>